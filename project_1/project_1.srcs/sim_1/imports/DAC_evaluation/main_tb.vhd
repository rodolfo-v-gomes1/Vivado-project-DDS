    library IEEE;
    use IEEE.STD_LOGIC_1164.all;          -- Bug 2 missing context clause
    
    entity MAIN_TB is
    end;
    
    architecture BENCH of MAIN_TB is      -- Bug 1 missing _
    
     -- Declaration of the component to be tested --
    entity design_1_wrapper is
       port (
         m_axis_data_tdata : out STD_LOGIC_VECTOR ( 15 downto 0 );
         sys_diff_clock_clk_n : in STD_LOGIC;
         sys_diff_clock_clk_p : in STD_LOGIC
       );
     end design_1_wrapper;
     
    
    -- Tesbench signals --
    signal Tclk_n, Tclk_p: STD_LOGIC; --CORRESPOND TO A AND B FROM MAIN
    signal Tdata : STD_LOGIC_VECTOR (15 DOWNTO 0);
    --signal Tclk: STD_LOGIC;
    constant clk_period : time := 1 ns;
    
    begin                                 -- Bug 2 begin in wrong place
     
     -- Port map between the VHDL sorce and its testbench -- 
     M: design_1_wrapper PORT MAP (
      sys_diff_clock_clk_n   => Tclk_n, --LIGAR
      sys_diff_clock_clk_p   => Tclk_p,
      m_axis_data_tdata   => Tdata
      --CLOCK_MAIN => Tclk
      );
      
        ---- Clock process definitions( clock with 50% duty cycle is generated here.
            clk_process :process
            begin
                 sys_diff_clock_clk_p <= '0';
                 wait for clk_period/2;  --for 0.5 ns signal is '0'.
                sys_diff_clock_clk_p <= '1';
              wait for clk_period/2;  --for next 0.5 ns signal is '1'.
     end process;
        
           -- Differential clock
     sys_clk_n <= ~sys_clk_p;
    
    process
    begin
    
    wait for 10 NS;
    
    wait for 10 NS;
    
    wait for 10 NS;
    
    wait for 10 NS;
    wait;
    end process;         -- Bug 6 missing semicolons
    
    end BENCH;
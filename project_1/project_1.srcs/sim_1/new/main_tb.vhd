library IEEE;
library UNISIM;

use IEEE.Std_logic_1164.all;
use IEEE.Numeric_std.all;
use UNISIM.VCOMPONENTS.ALL;
    
    entity MAIN_TB is
    end;
    
    architecture BENCH of MAIN_TB is      -- Bug 1 missing _
    
     -- Declaration of the component to be tested --
    component design_1_wrapper is
      port (
       sine : out STD_LOGIC_VECTOR ( 19 downto 0 );
       sys_diff_clock_clk_n : in STD_LOGIC;
       sys_diff_clock_clk_p : in STD_LOGIC
     );
     end component;
     
    
    -- Tesbench signals --
    signal Tclk_n, Tclk_p: STD_LOGIC; --CORRESPOND TO A AND B FROM MAIN
    signal Tdata : STD_LOGIC_VECTOR (19 DOWNTO 0);

    constant clk_period : time := 5 ns;
    
    begin                                 -- Bug 2 begin in wrong place
     
     -- Port map between the VHDL sorce and its testbench -- 
     M: design_1_wrapper PORT MAP (
      sys_diff_clock_clk_n   => Tclk_n, --LIGAR
      sys_diff_clock_clk_p   => Tclk_p,
      sine   => Tdata
      --CLOCK_MAIN => Tclk
      );
      
        ---- Clock process definitions( clock with 50% duty cycle is generated here.
            clk_process :process
            begin
                 Tclk_n <= '1';
                 Tclk_p <= '0';
                 wait for clk_period/2;  --for 0.5 ns signal is '0'.
                Tclk_n <= '0';
                Tclk_p <= '1';
              wait for clk_period/2;  --for next 0.5 ns signal is '1'.
     end process;
        
    
    process
    begin
    
    wait for 10 NS;
    
    wait for 10 NS;
    
    wait for 10 NS;
    
    wait for 10 NS;
    wait;
    end process;         -- Bug 6 missing semicolons
    
    end BENCH;
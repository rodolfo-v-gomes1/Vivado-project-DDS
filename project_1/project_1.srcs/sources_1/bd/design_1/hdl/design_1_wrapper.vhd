library IEEE;
library UNISIM;

use IEEE.Std_logic_1164.all;
use IEEE.Numeric_std.all;
use UNISIM.VCOMPONENTS.ALL;


entity design_1_wrapper is
  port (
    sine : out STD_LOGIC_VECTOR ( 19 downto 0 );
    sys_diff_clock_clk_n : in STD_LOGIC;
    sys_diff_clock_clk_p : in STD_LOGIC
  );
end design_1_wrapper;

architecture STRUCTURE of design_1_wrapper is
  component design_1 is
  port (
    S_AXIS_PHASE_tdata : in STD_LOGIC_VECTOR ( 15 downto 0 );
    S_AXIS_PHASE_tvalid : in STD_LOGIC;
    clk_out1 : out STD_LOGIC;
    m_axis_data_tdata : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m_axis_data_tvalid : out STD_LOGIC;
    sys_diff_clock_clk_n : in STD_LOGIC;
    sys_diff_clock_clk_p : in STD_LOGIC
  );
  end component design_1;

  signal clk : std_logic;
  signal count    : unsigned(15 downto 0) := (others => '0');
  signal s_sine   : std_logic_vector(19 downto 0) := (others => '0');
    
  signal s_axis_phase_tvalid, m_axis_data_tvalid    : std_logic;
   
  signal s_axis_phase_tdata_sine_high	: std_logic_vector(15 downto 0) := (others => '0');
  signal m_axis_data_tdata_sine_high    : std_logic_vector(23 downto 0) := (others => '0');  
  
  
begin

  s_axis_phase_tvalid <= '1';
  m_axis_data_tvalid <= '1';
 
  s_axis_phase_tdata_sine_high(15 downto 0) <= std_logic_vector(count(15 downto 0));
  s_sine <= m_axis_data_tdata_sine_high(19 downto 0);
  sine <= s_sine;
 
 process (clk)
   begin
     if rising_edge (clk) then
         count <= count + 12;
     end if;
   end process;

    design_1_i: component design_1
     port map (
      S_AXIS_PHASE_tdata => s_axis_phase_tdata_sine_high,
      S_AXIS_PHASE_tvalid => s_axis_phase_tvalid,
      clk_out1 => clk,
      m_axis_data_tdata => m_axis_data_tdata_sine_high,
      m_axis_data_tvalid => open,
      sys_diff_clock_clk_n => sys_diff_clock_clk_n,
      sys_diff_clock_clk_p => sys_diff_clock_clk_p
    );
    
    
end STRUCTURE;
